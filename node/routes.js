const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const product = mongoose.model('product');

router.route('/product').get((req, res, next) => {
    product.find({}, (err, products) => {
        if(err) return res.status(500).send('hiba!');
        res.status(200).send(products);
    })
}).post((req, res, next) => {
    if(req.body.id && req.body.name && req.body.price) {
        product.findOne({id: req.body.id}, (err, product) => {
            if(err) return res.status(500).send('hiba!');
            const prdct = new product({id: req.body.id, name: req.body.name, 
                price: req.body.price});
                prdct.save((error) => {
                if(error) return res.status(500).send('hiba!');
                return res.status(200).send(prdct);
            })
        })
    } else {
        return res.status(400).send('Hiányzik az id/name/price!');
    }
}).put((req, res, next) => {
    if(req.body.id && req.body.name && req.body.price) {
        product.findOne({id: req.body.id}, (err, product) => {
            if(err) return res.status(500).send('hiba!');
            if(product) {
                product.name = req.body.name;
                product.price = req.body.price;
                product.save((error) => {
                    if(error) return res.status(500).send('hiba!');
                    return res.status(200).send('Siker!');
                })
            } else {
                return res.status(400).send('Nincs ilyen id!');
            }
        })
    } else {
        return res.status(400).send('Hiányzik az id/name/price!');
    }
})

router.route('/product/:id').get((req,res) => {
    if(req.params.id) {
        product.findOne({id: req.params.id}, (err, product) => {
            if(err) return res.status(500).send('hiba!');
            res.status(200).send(product);
        })
    }
})

router.route('/product/:id').put((req,res) => {
    if(req.params.id && req.body.id && req.body.name && req.body.price) {
        product.findOne({id: req.params.id}, (err, product) => {
            if(err) return res.status(500).send('hiba!');
            if(product) {
                product.id = req.body.id;
                product.name = req.body.name;
                product.price = req.body.price;
                product.save((error) => {
                    if(error) return res.status(500).send('hiba!');
                    return res.status(200).send(product);
                })
            } else {
                return res.status(400).send('Nincs ilyen id!');
            }
        })
    } else {
        return res.status(400).send('Hiányzik az id/name/price!');
    }
})

router.route('/product/:id').delete((req, res)=>{
    if(req.params.id) {
        product.findOne({id: req.params.id}, (err, product) => {
            if(err) return res.status(500).send('hiba!');
            if(product) {
                product.delete((error) => {
                    if(error) return res.status(500).send('hiba!');
                    return res.status(200).send('Sikeres törlés!');
                })
            } else {
                return res.status(400).send('Nincs ilyen id!');
            }
        })
    } else {
        return res.status(400).send('Hiányzik az id!');
    }
  }
);

module.exports = router;