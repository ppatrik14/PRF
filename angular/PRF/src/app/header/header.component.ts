import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToProducts() {
    this.router.navigate(['/listProducts']);
  }
  goToTransactions() {
    this.router.navigate(['/listTransactions']);
  }
  logOut() {
    this.router.navigate(['/login']);
  }

}
