import { Component, Inject, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product';
import { Router } from '@angular/router';
import { TransactionService } from '../services/transaction.service';
import { Transaction } from '../models/transaction';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css'],
  providers: [NgbCarouselConfig]
})
export class ListProductsComponent implements OnInit {

  products: Product[] = [];
  transaction: Transaction;
  loading = true;
  loader: boolean = false;
  images = [26, 201, 312, 2, 124].map((n) => `https://picsum.photos/id/${n}/900/500`);

  constructor(config: NgbCarouselConfig, private _productService: ProductService, private _transactionService: TransactionService, private router: Router) {
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
   }

  ngOnInit(): void {
    this.loading = true;
        this._productService.getProducts()
          .subscribe(products => {
            this.loading = false;
            this.products = products;
          });
  }

  createNewTransaction(product: Product) {
    const transaction: Transaction = {
      product_id: product.id,
      date: new Date(),
      sum: product.price
    };

    this._transactionService.create(transaction)
      .subscribe((transaction: Transaction) => {
         this.loader = false;
      },
        (error) => {
          console.error(error);
          this.loader = false;
        });
  }

  goToAddNewProduct() {
    this.router.navigate(['/addNewProduct']);
  }
}
