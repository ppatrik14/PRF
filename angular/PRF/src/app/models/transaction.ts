export interface Transaction {
    id?: number;
    date: Date;
    sum: number;
    product_id?: number;
}
