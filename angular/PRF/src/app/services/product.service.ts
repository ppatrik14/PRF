import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PusherService } from './pusher.service';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { map } from 'rxjs/operators';
import { mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _endPoint = environment.springUrl;
  private _channel: any;

  constructor(private _http: HttpClient, private _pusherService: PusherService) {
    this._channel = this._pusherService.getPusher().subscribe('product');
   }

  /**
  * @return product's channel for the different event available under product
  */
  getChannel () {
    return this._channel;
  }

  getProducts(): Observable<Product[]> {
    return this._http.get(this._endPoint + "/products").pipe(map(res => <Product[]> res));
  }

  create(param: Product): Observable<Product> {
    return this._http.post(this._endPoint + "/product", param, {responseType: 'json'}).pipe(map(res => <Product> res));
  }

  delete(product: Product): Observable<Product> {
    return this._http.delete(this._endPoint + "/product" + product.id, {responseType: 'text'}).pipe(mapTo(product));
  }
}
