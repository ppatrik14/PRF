// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  custom: 'This is PRF dev',
  serverUrl: 'https://pf-webshop-be.herokuapp.com',
  springUrl:'https://calm-beach-21268.herokuapp.com/',
  firebase: {
    apiKey: "AIzaSyDipbRvmdfy4YCSNiQrmgQK_HXMrM6NQtI",
    authDomain: "pf-webshop.firebaseapp.com",
    projectId: "pf-webshop",
    storageBucket: "pf-webshop.appspot.com",
    messagingSenderId: "349867102500",
    appId: "1:349867102500:web:eadf92b79ca253370663b1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
