DROP TABLE IF EXISTS products;
CREATE TABLE products(id serial PRIMARY KEY, name VARCHAR(255), price INTEGER);
DROP SEQUENCE IF EXISTS hibernate_sequence;
CREATE SEQUENCE hibernate_sequence START 1;

DROP TABLE IF EXISTS transactions;
CREATE TABLE transactions(id serial PRIMARY KEY, date DATE, product_id INTEGER, sum INTEGER);
DROP SEQUENCE IF EXISTS hibernate_sequence_1;
CREATE SEQUENCE hibernate_sequence_1 START 1;