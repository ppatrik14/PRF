package hu.szte.prf.prfpelda.service;

import java.util.List;

import hu.szte.prf.prfpelda.models.Transaction;
import hu.szte.prf.prfpelda.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {

    TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public void addTransaction(Transaction transaction) {
        this.transactionRepository.save(transaction);
        
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return this.transactionRepository.findAll();
    }

    @Override
    public Transaction getTransactionById(int id) {
        return this.transactionRepository.findById(id).get();
    }

    @Override
    public void deleteTransactionById(int id) {
        this.transactionRepository.deleteById(id);
    }
}
