package hu.szte.prf.prfpelda.service;

import java.util.List;

import hu.szte.prf.prfpelda.models.Product;
import hu.szte.prf.prfpelda.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void addProduct(Product product) {
        this.productRepository.save(product);
    }

    @Override
    public void updateProduct(Product product) {
        Product product1 = this.productRepository.findById(product.getId()).get();
        product1.setPrice(product.getPrice());
        product1.setName(product.getName());
        this.productRepository.save(product1);
    }

    @Override
    public List<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    @Override
    public Product getProductById(int id) {
        return this.productRepository.findById(id).get();
    }

    @Override
    public void deleteProductById(int id) {
        this.productRepository.deleteById(id);
    }
    
}
