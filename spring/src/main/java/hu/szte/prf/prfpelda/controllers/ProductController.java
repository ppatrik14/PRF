package hu.szte.prf.prfpelda.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import hu.szte.prf.prfpelda.models.Product;
import hu.szte.prf.prfpelda.service.ProductService;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class ProductController {
    
    ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService= productService;
    }

    @PostMapping(path="/product", consumes = "application/json")
    public String newProduct(@RequestBody Product product) {
        try {
            this.productService.addProduct(product);
            return "Successful save";
        } catch (Exception e) {
            System.out.println(e);
            return "Error in create";
        }
    }

    @PutMapping(path="/product", consumes = "application/json")
    public String update(@RequestBody Product product) {
        try {
            this.productService.updateProduct(product);
            return "Successful update";
        } catch (Exception e) {
            System.out.println(e);
            return "Error in update";
        }
    }

    @GetMapping("/products")
    public List<Product> getAllProduct() {
        try {
            return this.productService.getAllProducts();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @GetMapping("/product")
    public Product getProductById(@RequestParam int id) {
        try {
            return this.productService.getProductById(id);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @DeleteMapping("/product")
    public String deleteProductById(@RequestParam int id) {
        try {
            this.productService.deleteProductById(id);;
            return "Successful delete";
        } catch (Exception e) {
            System.out.println(e);
            return "Error in delete";
        }
    }

}
