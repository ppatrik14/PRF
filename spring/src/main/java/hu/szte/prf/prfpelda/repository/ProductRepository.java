package hu.szte.prf.prfpelda.repository;

import hu.szte.prf.prfpelda.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer>{
    
}
