package hu.szte.prf.prfpelda.repository;

import hu.szte.prf.prfpelda.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>{
    
}
