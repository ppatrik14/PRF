package hu.szte.prf.prfpelda.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import hu.szte.prf.prfpelda.models.Transaction;
import hu.szte.prf.prfpelda.service.TransactionService;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class TransactionController {
    
    TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    
    @PostMapping(path="/transaction", consumes = "application/json")
    public Transaction newTransaction(@RequestBody Transaction transaction) {
        try {
            this.transactionService.addTransaction(transaction);;
            return transaction;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @GetMapping("/transactions")
    public List<Transaction> getAllTransaction() {
        try {
            return this.transactionService.getAllTransactions();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @GetMapping("/transaction")
    public Transaction getTransactionById(@RequestParam int id) {
        try {
            return this.transactionService.getTransactionById(id);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @DeleteMapping("/transaction")
    public String deleteTransactionById(@RequestParam int id) {
        try {
            this.transactionService.deleteTransactionById(id);;
            return "Successful delete";
        } catch (Exception e) {
            System.out.println(e);
            return "Error in delete";
        }
    }
}
